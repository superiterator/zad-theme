// jQuery for page scrolling feature - requires jQuery Easing plugin
$(function() {
    $('.page-scroll a').bind('click', function(event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: $($anchor.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Floating label headings for the contact form
$(function() {
    $("body").on("input propertychange", ".floating-label-form-group", function(e) {
        $(this).toggleClass("floating-label-form-group-with-value", !! $(e.target).val());
    }).on("focus", ".floating-label-form-group", function() {
        $(this).addClass("floating-label-form-group-with-focus");
    }).on("blur", ".floating-label-form-group", function() {
        $(this).removeClass("floating-label-form-group-with-focus");
    });
});

// Highlight the top nav as scrolling occurs
$('body').scrollspy({
    target: '.navbar-fixed-top'
})


//********sliders
$(document).ready(function($) {
$('.owl-carousel').owlCarousel({
		items:2,
		loop:true,
		margin:35,
		lazyLoad:true, //in this example only applied on video thumbnails
		merge: true, 
		video: true,
		responsive:{	
			480:{
				items:2
			},

			678:{
				items:3
			},

			960:{
				items:6
			}
		}
	});
});

$('.owl-carousel2').owlCarousel({
	animateOut: 'fadeOut',
 	items:1,
		loop:true,
		margin:35,
		lazyLoad:true, //in this example only applied on video thumbnails
		merge: true, 
});
