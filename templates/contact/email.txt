<html>
  
  <head>
    <meta charset="utf-8">
    <title>Zadi Platform Message</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link href="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="http://netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  </head>
  
  <body>
  <div class="container-fluid">

    
    <div class="row-fluid ">
        <div class="span8 offset2 ">
            <table class="table table-striped table-bordered table-hover">
                <tbody>
                    <tr>
                        <th style="text-align: center;">البيانات</th>
                        <th style="text-align: center;">الوصف</th>                    
                    </tr>
                    % if firstname:
                      <tr>
                          <td style="text-align: center; color:blue">${firstname}</td>
                          <td style="text-align: right; color:red">الاسم</td>
                      </tr>
                    % endif  
                    
                    % if lastname:
                    <tr>
                        <td style="text-align: center; color:blue">${lastname}</td>
                        <td style="text-align: right; color:red">اسم العائلة/اللقب</td>
                    </tr>
                    % endif

                    % if email:
                    <tr>
                        <td style="text-align: center; color:blue">${email}</td>
                        <td style="text-align: right; color:red">البريد الالكتروتي</td>
                    </tr>
                    % endif

                    % if profession:
                    <tr>
                        <td style="text-align: center; color:blue">${profession}</td>
                        <td style="text-align: right; color:red">التخصص</td>
                    </tr>
                    % endif

                    % if interest:
                    <tr>
                        <td style="text-align: center; color:blue">${interest}</td>
                        <td style="text-align: right; color:red">الاهتمام</td>
                    </tr>
                    % endif

                    % if instorg:
                    <tr>
                        <td style="text-align: center; color:blue">${instorg}</td>
                        <td style="text-align: right; color:red">المؤسسة / المنظمة التابعة</td>
                    </tr>
                    % endif

                    % if institution:
                    <tr>
                        <td style="text-align: center; color:blue">${institution}</td>
                        <td style="text-align: right; color:red">موقع المؤسسة </td>
                    </tr>
                    % endif

                    % if discipline:
                    <tr>
                        <td style="text-align: center; color:blue">${discipline}</td>
                        <td style="text-align: right; color:red">المجال</td>
                    </tr>
                    % endif

                    % if course_title:
                    <tr>
                        <td style="text-align: center; color:blue">${course_title}</td>
                        <td style="text-align: right; color:red">عنوان المساق</td>
                    </tr>
                    % endif

                    % if message:
                    <tr>
                        <td style="text-align: center; color:blue">${message}</td>
                        <td style="text-align: right; color:red">الرسالة</td>
                    </tr>
                    % endif
                    
                        
                        
                    </tbody>

                </table>
        </div>
    </div>
        
</div>
  </body>
</html>